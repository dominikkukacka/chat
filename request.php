<?php

include("bootstrap.php");

$pimple["slim"]->get("/channels", function() use ($pimple) {

    $channel_keys = $pimple["redis"]->sort("channels", array(
        "by" => "channels:*->order"
    ));
    $channels = array_map(function($k) use ( $pimple ) { 
        return $pimple["redis"]->hgetall("channels:$k"); 
    }, $channel_keys);
    $channels = array_combine($channel_keys, $channels);

    echo json_encode($channels);
});



$pimple["slim"]->get("/channels/:channel", function( $channel ) use ($pimple) {
    if(empty($_SESSION["name"]) || empty($_SESSION["avatar"])) {
        echo "not logged in";
        exit;
    }
    $ids = $pimple["redis"]->smembers("channels:$channel:backlog");
    $messages = array_map(function($id) use ($pimple) {
        return $pimple["redis"]->hgetall("messages:$id");
    }, $ids);

    echo json_encode($messages);
});


$pimple["slim"]->post("/channels/:channel", function( $channel ) use ($pimple) {
    if(empty($_SESSION["name"]) || empty($_SESSION["avatar"])) {
        echo "not logged in";
        exit;
    }
    
    $message = $_POST["message"];

    $id = $pimple["redis"]->incr("messages:id");
    $data = array(
        "id" => $id,
        "name" => $_SESSION["name"],
        "avatar" => $_SESSION["avatar"],
        "message" => $message,
    );

    $pimple["redis"]->hmset("messages:$id", $data);
    $pimple["redis"]->sadd("channels:$channel:backlog", $id);
    $pimple["redis"]->publish("channels:$channel:stream", json_encode($data));

    echo $id;

});

$pimple["slim"]->get("/channels/:channel/subscribe", function($channel) use ($pimple) {

    set_time_limit(0);
    ini_set('default_socket_timeout', -1);
    ini_set('zlib.output_compression', 0); 
    ini_set('implicit_flush', 1);
    ini_set('output_buffering', "off");

    ignore_user_abort(); 

    //function f($redis, $chan, $msg) {
    for($i=0;$i<10;$i++) {
        echo "asdasdasda\n";
        flush_buffers();
        sleep(1);
    }
    //}

    //$pimple["redis"]->subscribe(array("channels:$channel:stream"), 'f');

});


$pimple["slim"]->run();


function flush_buffers() {
    ob_flush();
    flush();
    ob_end_flush();
    ob_start();
}
