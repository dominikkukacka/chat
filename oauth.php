<?php

include("bootstrap.php");

$config = include("oauth_config.php");
$hybridauth = new Hybrid_Auth( $config );
if(isset($_GET["is_loggedin"])) {
    try {
        $bb = $hybridauth->getAdapter( "Bitbucket" );
    } catch (Exception $e) {
        echo 0;
    }
    if($hybridauth->isConnectedWith("Bitbucket") === true) {

        $user_profile = $bb->getUserProfile();
        if(null !== $user_profile) {

            $_SESSION["name"] = $user_profile->first_name;
            $_SESSION["avatar"] = $user_profile->avatar;
            echo json_encode(array(
                "first_name" => $user_profile->first_name,
                "avatar" => $user_profile->avatar,
            ));
        } else {
            session_destroy();
            header("Location: oauth.php");
            echo 0;
        }

    } else {
        echo 0;
    };
} else {
    try {
        $bb = $hybridauth->authenticate( "Bitbucket" );
    } catch (Exception $e) {
        ?>
        <script>
            window.close();
        </script>
        <?php
        exit;
    }
    $user_profile = $bb->getUserProfile();
    if(null !== $user_profile) {

        //echo "<pre>".print_r($user_profile,true)."</pre>";
        $_SESSION["name"] = $user_profile->first_name;
        $_SESSION["avatar"] = $user_profile->avatar;
        $data = json_encode(array(
            "first_name" => $user_profile->first_name,
            "avatar" => $user_profile->avatar,
        ));
        ?>

        <script>
            window.opener.Chat.loggedIn(<?= $data ?>);
            window.close();
        </script>
        <?php
    } else {
        session_destroy();
        ?>
        <script>
            window.close();
        </script>
        <?php
    }
}
