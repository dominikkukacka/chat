<?php
/*!
* HybridAuth
* http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
* (c) 2009-2012, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
*/

// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------

if(gethostname() === "localtest") {
    define("OAUTH_BASE_URL", "http://alphadev.local/workspace/chat/vendor/hybridauth/hybridauth/hybridauth/");
} elseif(gethostname() === "precise64") {
    define("OAUTH_BASE_URL", "http://dev.creativebrains.net/chat/callback/");
} else {
    define("OAUTH_BASE_URL", "http://lab.codecookie.net/chat/callback/");
}

return 
	array(
		"base_url" => OAUTH_BASE_URL, 

		"providers" => array ( 

			"Twitter" => array ( 
				"enabled" => true,
				"keys"    => array ( "key" => "AnT2DTHILHSysosa9QBdFw", "secret" => "hczoo6H1iJTMxe3zLl3VE1eEl4DuDnreJ9szlHurCXs" ) 
			),

            "Github" => array (
                "enabled" => true,
                "keys"    => array ( "id" => "dbade961a6dde662697e", "secret" => "95be42f209504c42e74ce5f32ba3a802de7d2136" ) 
            ),

			"Bitbucket" => array (
				"enabled" => true,
				"keys"    => array ( "key" => "sPtJra3Sf9LsP98m4c", "secret" => "gThSNjSK4tGBQWD9MJQeUtFBy75Lxapk" ) 
			),
		),

		// if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
		"debug_mode" => false,

		"debug_file" => __DIR__ . "/test.log",
	);
