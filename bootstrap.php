<?php
ini_set("display_errors", true);
error_reporting(E_ALL);

ini_set("session.save_handler", "redis");
ini_set("session.save_path", "tcp://127.0.0.1:6379?prefix=chat:&database=2");

ini_set("session.gc_maxlifetime", 24 * 3600 * 30 * 4);  

$currentCookieParams = session_get_cookie_params(); 
session_set_cookie_params( 
    24 * 3600 * 30 * 4, 
    $currentCookieParams["path"], 
    $currentCookieParams["domain"], 
    $currentCookieParams["secure"],
    false
); 

session_start();

include("vendor/autoload.php");

$pimple = new \Pimple();

$pimple["redis"] = $pimple->share(function() {

    $redis = new Redis();
    $redis->connect("127.0.0.1");
    $redis->select(1);
    return $redis;

});

$pimple["slim"] = $pimple->share(function() {
    return new \Slim\Slim();
});