<?php
/*!
* HybridAuth
* http://hybridauth.sourceforge.net | https://github.com/hybridauth/hybridauth
*  (c) 2009-2011 HybridAuth authors | hybridauth.sourceforge.net/licenses.html
*/

/**
 * Hybrid_Providers_GitHub 
 */
class Hybrid_Providers_Bitbucket extends Hybrid_Provider_Model_OAuth1 { 

    /**
    * IDp wrappers initializer 
    */
    function initialize() {
        parent::initialize();

        $this->api->api_base_url      = "https://api.bitbucket.org/1.0/";
        $this->api->authorize_url     = "https://bitbucket.org/!api/1.0/oauth/authenticate";
        $this->api->request_token_url = "https://bitbucket.org/!api/1.0/oauth/request_token";
        $this->api->access_token_url  = "https://bitbucket.org/!api/1.0/oauth/access_token";

    }

    /**
    * load the user profile from the IDp api client
    */
    function getUserProfile() {
        $data = $this->api->api( "user" ); 
        if($data !== null) {
            return $data->user;
        } else {
            return null;
        }
    }

}
