<?php

include("bootstrap.php");

$channel = $_GET["channel"];

if(empty($_SESSION["name"]) || empty($_SESSION["avatar"])) {
    echo "not logged in";
    exit;
}

set_time_limit(0);
ini_set('default_socket_timeout', -1);
ini_set('zlib.output_compression', 0); 
ini_set('implicit_flush', 1);
ini_set('output_buffering', "off");

ignore_user_abort(); 

function f($redis, $chan, $msg) {
    echo "$msg\n";
    flush_buffers();
    exit;
}

$pimple["redis"]->subscribe(array("channels:$channel:stream"), 'f');
function flush_buffers() {
    ob_flush();
    flush();
    ob_end_flush();
    ob_start();
}