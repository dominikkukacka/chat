var Chat = {


    active_channel: "lobby",
    user_data: null,

    init: function() {

        $("#loading-modal").reveal({
            closeOnBackgroundClick: false
        });

        Chat.checkLogin();
       
    },

    checkLogin: function(callback) {
        $.get("oauth.php?is_loggedin",function(data){
            try {
                data = JSON.parse(data);
                if(data.first_name && data.avatar) {
                    Chat.loggedIn(data);
                } else {
                    Chat.auth();
                }
            } catch( e ) {
                console.warn("dafuq? o.O");
            }
        });
    },

    activateChannel: function( channel ) {

        Chat.active_channel = channel;
        $(".channels li").removeClass("active");
        $(".channels li[data-id='"+channel+"']").addClass("active");

        Chat.loadHistory( channel );
        Chat.subscribeChannel( channel );
    },

    loadHistory: function( channel ) {
        Chat.clear();
        $.getJSON("request.php/channels/" + channel, function(data) {
            $(".loading").remove();
            for(var i in data) {
                var message = data[i];
                console.log(message);
                Chat.addMessage(message);
            }
            Chat.scrollToBottom(true);
        });
    },

    clear: function() {
        $(".messages").html("<div class='row loading'><div class='twelve columns'>loading...</div></div>");
    },

    subscribeChannel: function( channel ) {
        Connection.disconnect();
        Connection.init(channel);
    },

    addMessage: function( message ){
        console.log(message);
            var id = message.id,
            text   = message.message,
            name   = message.name,
            avatar = message.avatar;

        var message = $(".message.template").clone();

        message.removeClass("template");

        message.find("p").html(text);
        message.find(".name").html(name);
        message.find(".image").attr("src", avatar);

        $(".messages").append(message);

        Chat.scrollToBottom();
    },
    scrollToBottom: function( everytime ) {

        if( 
            everytime === true
            || ( window.innerHeight + document.body.scrollTop  >= document.body.offsetHeight - 110 )
        ) {
            $(document).scrollTop($(document).height());
        }

    },

    loggedIn: function(data) {
        $('#loading-modal').trigger('reveal:close');

        Chat.user_data = data;

        if( "" !== window.location.hash.substr(1) ) {
            Chat.activateChannel(window.location.hash.substr(1));
        }  else {
            Chat.activateChannel("lobby");
        } 
    },

    auth: function() {
        var url = "oauth.php", 
            width = 1030, 
            height = 700;
        
        var win = window.open(url, "Auth", "width=" + width + ",height=" + height + ",resizable=no,menubar=no,location=no,directories=no,scrollbars=no,toolbar=no");
        win.moveTo((screen.width/2)-(width/2),(screen.height/2)-(height/2)); 
        win.focus();

        return true;
    }

};


var Connection = {

    xhr: null,
    pollTimer: null,
    nextReadPos: 0,

    is_aborted: false,
    
    init: function( channel ) {

        Connection.xhr = $.ajax({
            url: "subscribe.php?channel=" + channel,
            timeout: 60 * 1000
        })
        .done(function(d) { 
            var data = JSON.parse(d);
            Chat.addMessage(data); 
            
        })
        .always(function() {
            if(Connection.is_aborted !== true) {
                Connection.reset(channel); 
            }
        });

    },

    disconnect: function() {
        if(Connection.xhr !== null) {
            Connection.is_aborted = true;
            Connection.xhr.abort();
            Connection.is_aborted = false;
            Connection.xhr = null;
        }
    },

    reset: function(channel) {
        Connection.disconnect();
        Connection.init( channel );
    },

    sendMessage: function( message ) {
        $.post("request.php/channels/" + Chat.active_channel, {
            message: message
        }, function(data){
            //console.log(data);
        })
    }
};

var UI = {
    sendMessage: function() {
        Connection.sendMessage($(".send input").val());
        $(".send input").val("");
    }
}

$(function(){

    Chat.init();

 
    $(document).on("click", ".channels a", function() {
        var hash = $(this).attr("href").substr(1);
        Chat.activateChannel(hash);
    });

    $(window).scroll(function(e){
        $(".flow").animate({ marginTop: $(window).scrollTop()},1);
    });

    $.getJSON("request.php/channels", function(channels) {
            $(".channels").html("");

            for(var k in channels) {
                var channel = channels[k],
                    a = $("<a>").html(channel.name).attr("href", "#" + k),
                    li = $("<li>").append(a).attr("data-id", k);

                if(k === Chat.active_channel) {
                    li.addClass("active");
                }

                $(".channels").append(li);
            }
    });


    $(document).on("click", "#send_message", function() {
        UI.sendMessage();
    });
    $(document).on("keyup", ".send input", function(e) {
        if(e.keyCode === 13) {
            UI.sendMessage();
        }
    });
    $(document).on("click", "#attach_item", function() {
        console.log("attach");

        $('#oauth-modal').reveal({
             animation: 'fadeandpop', //fade, fadeandpop, none
             animationspeed: 300, //how fast animations are
             closeonbackgroundclick: true, //if you click background will modal close?
             dismissmodalclass: 'close-reveal-modal' //the class of a button or element that will close an open modal
        });
                    
    });


});